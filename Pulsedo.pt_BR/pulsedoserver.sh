#!/data/data/com.termux/files/usr/bin/bash
##############################################
# PulsedoServer                              #
# Versão 1.2.3                               #
#                                            #
# Este script é parte do Pulsedo.            #
# Cledson Cavalcanti (c) 2019 - 2024         #
# cledsonitgames@gmail.com                   #
##############################################

PulsedoVer=1.2.3
PulsedoCfgFolder=$HOME/.config/PulsedoServer
Copynote="por Ledso! (c) 2019 - 2024"

function PulsedoHelp() {
    echo "USE: $0 [--start | --stop | -k, kill-server | -r, --remove-def-ip | -h, --help]"
    echo ""
    echo "DESCRIÇÕES DAS OPÇÕES"
    echo "          -p, --start - carrega o módulo native-protocol-tcp do PulseAudio para reproduzir os sons dos dispositivos da sua rede local."
    echo "           -s, --stop - descarrega o módulo native-protocol-tcp do PulseAudio para não reproduzir os sons dos dispositivos da sua rede local."
    echo "    -k, --kill-server - gentilmente, mata o servidor PulseAudio."
    echo "           -h, --help - mostra este texto de ajuda."
    echo ""
    echo "PulsedoClient versão $PulsedoVer."
    echo "by Lesdo Upper (também conhecido como: Ledso, Cledson, Tales)"
    echo "Envie um e-mail para cledsonitgames@gmail.com se tiver problemas."
}

case $1 in
    -p|--start)
        ip=$((ip -h -f inet address || ifconfig) 2>/dev/null | grep -E '^([0-9]. )?(ap[0-9]|wl(an|p[0-9]s))' -A 1 | sed -En 's/.*inet (([0-9]+\.?){4}).*/\1/p')
        if ! (echo "$ip" | grep -E '(([0-9]+\.){3}([0-9]+))' 1>/dev/null); then
            echo "IP da rede não disponível. Verifique se seu dispositivo está conectado ao WiFi ou há um ponto de acesso ativo!"
            exit 1
        fi

        pulseaudio --check
        if [ $? -eq 1 ]; then
            pulseaudio --start --exit-idle-time=-1
            error_code=$?
            if [ $error_code -ne 0 ]; then
                echo "Tenha certeza que o PulseAudio está corretamente instalado e configurado."
                echo "Você pode, gentilmente, matá-lo com \"pulseaudio -k\" pra ter certeza que está tudo bem."
                exit $error_code
            fi
        fi
        pacmd load-module module-native-protocol-tcp auth-ip-acl=$ip auth-anonymous=1
        if [ $? -ne 0 ]; then
            echo "Falha ao iniciar!"
            exit 1
        else
            echo "Servidor escutando no endereço $ip."
        fi
        ;;

    -s|--stop)
        pacmd unload-module module-native-protocol-tcp
        echo "Mate o servidor PulseAudio se estiver utilizando este script no Termux!"
        echo "Você pode fazer isso executando o comando '$0 kill-server'" ;;

    -k|--kill-server)
        pulseaudio -k
        if [ $? -eq 1 ]; then
            echo "Falhou ao encerrar o servidor gentilmente. Deseja encerrar com grosseria? [S/n]"
            read response
            if [ $response == "S" -o $response == "s" ]; then
                pkill pulseaudio
                exs=$?
                if [ $exs -ne 0 ]; then
                    echo "Verifique sua conta antes de matar o PulseAudio e tente novamente."
                    exit $exs
                fi
            fi
        fi
        ;;

    *)
        PulsedoHelp
        ;;
esac
