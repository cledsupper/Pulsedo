#!/usr/bin/env bash
##############################################
# PulsedoClient                              #
# Versão 1.2.3                               #
#                                            #
# Este script é parte do Pulsedo.            #
# Cledson Cavalcanti (c) 2019 - 2024         #
# cledsonitgames@gmail.com                   #
##############################################

PulsedoVer=1.2.3
PulsedoCfgFolder=$HOME/.config/PulsedoClient
Copynote="por Ledso! (c) 2019 - 2024"

function PulsedoHelp() {
    echo "USE: $0 [<-s | --start> | <-r | --remove-def-ip> | <-h | --help>]"
    echo ""
    echo "DESCRIÇÕES DAS OPÇÕES"
    echo "          -p, --start - Adiciona a saída de áudio remota."
    echo "           -s, --stop - Remove a saída de áudio remota."
    echo "  -r, --remove-def-ip - remove o endereço IP que você aceitou salvar como padrão, assim este script volta a te perturbar de novo pelo IP."
    echo "           -h, --help - mostra este texto de ajuda."
    echo ""
    echo "PulsedoClient versão $PulsedoVer"
    echo $Copynote
    echo "Envie um e-mail para cledsonitgames@gmail.com se tiver problemas."
}

case $1 in
    -p|--start)
        ls $PulsedoCfgFolder/defip 2> /dev/null 1> /dev/null
        ls_return=$?
        if [ $ls_return -ne 0 ]; then
            echo "Digite o IP do PulsedoServer (o mesmo IP onde o PulsedoServer está executando): [exemplo: 192.168.0.132]"
            read dev_ip

            echo "Deseja tornar o IP padrão? Ao fazer isso, este script não lhe pertubará mais."
            echo "Você pode deletar o IP padrão executando '$0 -r' em um terminal. [s/N]"
            read uoption

            if [ "$uoption" == "S" -o "$uoption" == "s" ]; then
                mkdir -p $PulsedoCfgFolder
                echo "$dev_ip" > $PulsedoCfgFolder/defip
                echo "$dev_ip configurado como IP padrão!"
            fi
        else
            dev_ip=`cat $PulsedoCfgFolder/defip`
        fi

        module=`pactl load-module module-tunnel-sink sink_name="pulsedo" sink_properties='device.description="Pulsedo"' server="$dev_ip" channels=2`
        if [ -n "$module" ]; then
            mkdir -p $PulsedoCfgFolder
            echo "$module" > $PulsedoCfgFolder/current_module
            echo "Conectado! caso haja falha na rede, pode ser necessário reconectar."
            notify-send -a PulsedoClient "Status da conexão" "conectado! casa haja falha na rede, pode ser necessário reconectar."
            pactl set-default-sink pulsedo
        else
            echo "Falha de conexão. Reconfigure o IP do servidor e tente novamente."
            notify-send -a PulsedoClient "Status da conexão" "Falha de conexão. Reconfigure o IP do servidor e tente novamente."
            exit 1
        fi
        ;;

    -s|--stop)
        module=`cat $PulsedoCfgFolder/current_module`
        if [ -n "$module" ]; then
            pactl unload-module "$module"
            rm $PulsedoCfgFolder/current_module
            echo "Desconectado!"
            notify-send -a PulsedoClient "Status da conexão" "desconectado!"
        else
            echo "Provavelmente desconectado."
            notify-send -a PulsedoClient "Status da conexão" "provavelmente desconectado!"
            exit 1
        fi
        ;;

    -r|--remove-def-ip)
        rm $PulsedoCfgFolder/defip 2> /dev/null
        ls $PulsedoCfgFolder/defip 1> /dev/null 2> /dev/null
        if [ $? -ne 0 ]; then
            echo "IP padrão removido!"
        else
            echo "Erro ao tentar remover IP. Por favor, cheque as permissões para escrita de 'defip' em $PulsedoCfgFolder e tente novamente."
        fi
        ;;

    help|-h|--help)
        PulsedoHelp
        ;;

    *)
        # Nothing seems like a help ask
        PulsedoHelp
        exit 2
        ;;
esac
