#!/usr/bin/env bash
##############################################
# PulsedoClient                              #
# Version 1.1                                #
#                                            #
# This script is part of Pulsedo.            #
# Cledson Cavalcanti (c) 2019 - 2021         #
# cledsonitgames@gmail.com                   #
# 🐘 @cledson_cavalcanti@mastodon.technology #
##############################################

PulsedoVer=1.1
PulsedoCfgFolder="$HOME/.config/PulsedoClient"
Copynote="by Ledso! (c) 2019 - 2021"

function PulsedoHelp() {
    echo "USE: $0 [<command> | <-r |--remove-def-ip> | <-h | --help>]"
    echo ""
    echo "OPTIONS"
    echo "            <command> - a command or program name to be runned with Pulsedo. That is, with the environment variable PULSE_SERVER set."
    echo "  -r, --remove-def-ip - remove the IP address you had accepted to save as default, so the script will prompt you for IP again."
    echo "           -h, --help - show this help text."
    echo ""
    echo "PulsedoClient version $PulsedoVer"
    echo $Copynote
    echo "Contact cledsonitgames@gmail.com for questions."
    echo "🐘 @cledson_cavalcanti@mastodon.technology"
}

ls $PulsedoCfgFolder/defip 2> /dev/null 1> /dev/null
ls_return=$?
run_command=0

case $1 in
    -r|--remove-def-ip)
        rm $PulsedoCfgFolder/defip 2> /dev/null
        ls $PulsedoCfgFolder/defip 1> /dev/null 2> /dev/null
        if [ $? -ne 0 ]; then
            echo "Default IP removed!"
        else
            echo "Error when trying to remove saved IP. Please check 'defip' for file write permissions at $PulsedoCfgFolder and try again."
        fi
        ;;

    help|-h|--help)
        PulsedoHelp
        ;;

    *)
        # Nothing seems like a help ask
        if [ "$1" == "" ]; then
            PulsedoHelp
        else
            run_command=1
        fi
        ;;
esac

if [ $run_command -eq 0 ]; then
    exit 0
fi

if [ $ls_return -ne 0 ]; then
    echo "Type the IP address of PulsedoServer (the same device's IP where PulsedoServer is running): [e.g.: 192.168.0.132]"
    read dev_ip

    echo "Do you want to set this IP as default? When doing it, Pulsedo won't ask you anymore."
    echo "You can delete the default IP typing '$0 -r' on shell. [Y/n]"
    read uoption

    if [ "$uoption" == "Y" -o "$uoption" == "y" ]; then
        mkdir -p $PulsedoCfgFolder
        echo "$dev_ip" > $PulsedoCfgFolder/defip
        echo "$dev_ip set as default!"
    fi
else
	dev_ip=`cat $PulsedoCfgFolder/defip`
fi

sh_cmd=$@

env PULSE_SERVER=$dev_ip $sh_cmd &
echo "You need to quit this program before shutting PulsedoServer down."
